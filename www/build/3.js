webpackJsonp([3],{

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateOrderClientPageModule", function() { return CreateOrderClientPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__create_order_client__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_breadcrumbs_breadcrumbs_module__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_order_header_order_header_module__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_order_footer_order_footer_module__ = __webpack_require__(299);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var CreateOrderClientPageModule = /** @class */ (function () {
    function CreateOrderClientPageModule() {
    }
    CreateOrderClientPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__create_order_client__["a" /* CreateOrderClientPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__create_order_client__["a" /* CreateOrderClientPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_breadcrumbs_breadcrumbs_module__["a" /* BreadcrumbsComponentModule */],
                __WEBPACK_IMPORTED_MODULE_4__components_order_header_order_header_module__["a" /* OrderHeaderComponentModule */],
                __WEBPACK_IMPORTED_MODULE_5__components_order_footer_order_footer_module__["a" /* OrderFooterComponentModule */]
            ],
        })
    ], CreateOrderClientPageModule);
    return CreateOrderClientPageModule;
}());

//# sourceMappingURL=create-order-client.module.js.map

/***/ }),

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BreadcrumbsComponentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngx_translate_core__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__breadcrumbs__ = __webpack_require__(296);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var BreadcrumbsComponentModule = /** @class */ (function () {
    function BreadcrumbsComponentModule() {
    }
    BreadcrumbsComponentModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__breadcrumbs__["a" /* BreadcrumbsComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* IonicModule */],
                __WEBPACK_IMPORTED_MODULE_0__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_4__breadcrumbs__["a" /* BreadcrumbsComponent */]
            ]
        })
    ], BreadcrumbsComponentModule);
    return BreadcrumbsComponentModule;
}());

//# sourceMappingURL=breadcrumbs.module.js.map

/***/ }),

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BreadcrumbsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BreadcrumbsComponent = /** @class */ (function () {
    function BreadcrumbsComponent(navCtrl) {
        this.navCtrl = navCtrl;
        console.log('Hello BreadcrumbsComponent Component');
    }
    BreadcrumbsComponent.prototype.goToScreen = function (screenNumber, bool) {
        var _this = this;
        switch (screenNumber) {
            case 1:
                if (screenNumber != this.pageNumber)
                    this.navCtrl.push('CreateOrderClientPage', {}, { animate: false }).then(function () {
                        var startIndex = _this.navCtrl.getActive().index - 1;
                        _this.navCtrl.remove(startIndex, 1);
                    });
                break;
            case 2:
                if (screenNumber != this.pageNumber)
                    this.navCtrl.push('CreateOrderProductPage', {}, { animate: false }).then(function () {
                        var index = _this.navCtrl.getActive().index;
                        _this.navCtrl.remove(0, index);
                    });
                break;
            case 3:
                if (screenNumber != this.pageNumber)
                    this.navCtrl.push('CreateOrderPaymentPage', {}, { animate: false }).then(function () {
                        var index = _this.navCtrl.getActive().index;
                        _this.navCtrl.remove(0, index);
                    });
                break;
            case 4:
                if (screenNumber != this.pageNumber)
                    this.navCtrl.push('CreateOrderLogisticPage', {}, { animate: false }).then(function () {
                        var index = _this.navCtrl.getActive().index;
                        _this.navCtrl.remove(0, index);
                    });
                break;
            default:
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], BreadcrumbsComponent.prototype, "pageNumber", void 0);
    BreadcrumbsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'breadcrumbs',template:/*ion-inline-start:"/Users/dongjunsong/Documents/Projects/JCL/git/jcladmin/src/components/breadcrumbs/breadcrumbs.html"*/'<ul class="steps-progress-bar">\n  <li id="first-step"  [ngClass]=" [(pageNumber == 1 ) ? \'active\' :\'\' ]" (click)="goToScreen(1 , $event.target.classList.contains(\'active\'))" >\n  </li>\n  <li id="second-step" [ngClass]="[(pageNumber == 2 ) ? \'active\' :\'\']" (click)="goToScreen(2,$event.target.classList.contains(\'active\'))" >\n  </li>\n  <li id="third-step" [ngClass]="[(pageNumber == 3) ? \'active\' : \'\']" (click)="goToScreen(3,$event.target.classList.contains(\'active\'))"  >\n  </li>\n  <li id="fourth-step" [ngClass]="[(pageNumber == 4) ? \'active\' : \'\' ]" (click)="goToScreen(4,$event.target.classList.contains(\'active\'))" >\n  </li> \n</ul>\n\n'/*ion-inline-end:"/Users/dongjunsong/Documents/Projects/JCL/git/jcladmin/src/components/breadcrumbs/breadcrumbs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]])
    ], BreadcrumbsComponent);
    return BreadcrumbsComponent;
}());

//# sourceMappingURL=breadcrumbs.js.map

/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderHeaderComponentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngx_translate_core__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__order_header__ = __webpack_require__(298);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var OrderHeaderComponentModule = /** @class */ (function () {
    function OrderHeaderComponentModule() {
    }
    OrderHeaderComponentModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__order_header__["a" /* OrderHeaderComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* IonicModule */],
                __WEBPACK_IMPORTED_MODULE_0__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_4__order_header__["a" /* OrderHeaderComponent */]
            ]
        })
    ], OrderHeaderComponentModule);
    return OrderHeaderComponentModule;
}());

//# sourceMappingURL=order-header.module.js.map

/***/ }),

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderHeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OrderHeaderComponent = /** @class */ (function () {
    function OrderHeaderComponent(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    OrderHeaderComponent.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HeaderPage');
    };
    OrderHeaderComponent.prototype.goToHomePage = function () {
        var self = this;
        self.navCtrl.setRoot('TabsPage');
        self.navCtrl.popToRoot({ animate: false });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], OrderHeaderComponent.prototype, "headerTitle", void 0);
    OrderHeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'order-header',template:/*ion-inline-start:"/Users/dongjunsong/Documents/Projects/JCL/git/jcladmin/src/components/order-header/order-header.html"*/' <ion-navbar hideBackButton = "true"> \n  <ion-buttons start>\n    <button ion-button icon-only (click)="goToHomePage()" >\n      <ion-icon ios="ios-home-outline"></ion-icon>\n    </button>\n  </ion-buttons>  \n  <ion-title > {{headerTitle}} </ion-title>  \n</ion-navbar>   '/*ion-inline-end:"/Users/dongjunsong/Documents/Projects/JCL/git/jcladmin/src/components/order-header/order-header.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], OrderHeaderComponent);
    return OrderHeaderComponent;
}());

//# sourceMappingURL=order-header.js.map

/***/ }),

/***/ 299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderFooterComponentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngx_translate_core__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__order_footer__ = __webpack_require__(300);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var OrderFooterComponentModule = /** @class */ (function () {
    function OrderFooterComponentModule() {
    }
    OrderFooterComponentModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__order_footer__["a" /* OrderFooterComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* IonicModule */],
                __WEBPACK_IMPORTED_MODULE_0__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_4__order_footer__["a" /* OrderFooterComponent */]
            ]
        })
    ], OrderFooterComponentModule);
    return OrderFooterComponentModule;
}());

//# sourceMappingURL=order-footer.module.js.map

/***/ }),

/***/ 300:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderFooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OrderFooterComponent = /** @class */ (function () {
    function OrderFooterComponent(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    OrderFooterComponent.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FooterPage');
    };
    OrderFooterComponent.prototype.goToNextScreen = function () {
        console.log(this.nextScreen);
        this.navCtrl.push(this.nextScreen, {}, { animate: false });
    };
    OrderFooterComponent.prototype.goToPreviousScreen = function () {
        switch (this.nextScreen) {
            case "CreateOrderPaymentPage":
                this.navCtrl.push('CreateOrderClientPage', {}, { animate: false });
                break;
            case "CreateOrderPaymentPage":
                this.navCtrl.push('CreateOrderClientPage', {}, { animate: false });
                break;
            case "NoPage":
                this.navCtrl.push('CreateOrderPaymentPage', {}, { animate: false });
                break;
            case "CreateOrderLogisticPage":
                this.navCtrl.push('CreateOrderProductPage', {}, { animate: false });
                break;
            default:
                this.navCtrl.push('CreateOrderClientPage', {}, { animate: false });
        }
    };
    OrderFooterComponent.prototype.showPrevious = function () {
        if (this.nextScreen == "CreateOrderProductPage")
            return false;
        else
            return true;
    };
    OrderFooterComponent.prototype.showNext = function () {
        if (this.nextScreen == "NoPage")
            return false;
        else
            return true;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], OrderFooterComponent.prototype, "nextScreen", void 0);
    OrderFooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'order-footer',template:/*ion-inline-start:"/Users/dongjunsong/Documents/Projects/JCL/git/jcladmin/src/components/order-footer/order-footer.html"*/'<ion-toolbar no-padding>\n  <ion-grid no-padding>\n    <ion-row>\n      <ion-col no-padding>\n        <button ion-button icon-start icon-only color="dark" (click)="goToPreviousScreen()" outline class="no-border" *ngIf="showPrevious()">\n          <ion-icon name="arrow-back"></ion-icon>\n        </button>\n      </ion-col>\n      <ion-col no-padding>\n        <button float-right ion-button icon-end icon-only color="dark" (click)="goToNextScreen()" *ngIf="showNext()" outline class="no-border">\n          <ion-icon name="arrow-forward"></ion-icon>\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-toolbar>\n '/*ion-inline-end:"/Users/dongjunsong/Documents/Projects/JCL/git/jcladmin/src/components/order-footer/order-footer.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], OrderFooterComponent);
    return OrderFooterComponent;
}());

//# sourceMappingURL=order-footer.js.map

/***/ }),

/***/ 301:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateOrderClientPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CreateOrderClientPage = /** @class */ (function () {
    function CreateOrderClientPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pageNumber = "1";
        this.headerTitle = "客户";
    }
    CreateOrderClientPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreateOrderClientPage');
    };
    CreateOrderClientPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-create-order-client',template:/*ion-inline-start:"/Users/dongjunsong/Documents/Projects/JCL/git/jcladmin/src/pages/create-order-client/create-order-client.html"*/' \n<ion-header> \n  <order-header [headerTitle]="headerTitle"> \n  </order-header>\n  <breadcrumbs [pageNumber]="pageNumber"></breadcrumbs>  \n</ion-header>  \n<ion-content padding> \n</ion-content>\n<ion-footer>\n  <order-footer nextScreen ="CreateOrderProductPage"></order-footer> \n</ion-footer>'/*ion-inline-end:"/Users/dongjunsong/Documents/Projects/JCL/git/jcladmin/src/pages/create-order-client/create-order-client.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], CreateOrderClientPage);
    return CreateOrderClientPage;
}());

//# sourceMappingURL=create-order-client.js.map

/***/ })

});
//# sourceMappingURL=3.js.map