import { Component } from '@angular/core';
import { IonicPage,ViewController, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TvaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tva',
  templateUrl: 'tva.html',
})
export class TvaPage {
  public tvaInfo : any;
  constructor(public navCtrl: NavController, 
    public viewCtrl : ViewController,
    public navParams: NavParams) {
    this.tvaInfo = {};
    this.tvaInfo.valid = false;
    this.tvaInfo.query = "";
    this.tvaInfo.country_code = "";
    this.tvaInfo.company_name = "";
    this.tvaInfo.company_address = "";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TvaPage');
  }
  ngOnInit(): void {
    this.tvaInfo = this.navParams.get('tva'); 
  } 
  close(){
    this.viewCtrl.dismiss( );
  }
}
 