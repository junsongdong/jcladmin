import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TvaPage } from './tva';

@NgModule({
  declarations: [
    TvaPage,
  ],
  imports: [
    IonicPageModule.forChild(TvaPage),
  ],
})
export class TvaPageModule {}
