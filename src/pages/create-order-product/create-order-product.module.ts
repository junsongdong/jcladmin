import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateOrderProductPage } from './create-order-product';
import { BreadcrumbsComponentModule } from '../../components/breadcrumbs/breadcrumbs.module';
import { OrderHeaderComponentModule } from '../../components/order-header/order-header.module';
import { OrderFooterComponentModule } from '../../components/order-footer/order-footer.module';
 
@NgModule({
  declarations: [
    CreateOrderProductPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateOrderProductPage),
    BreadcrumbsComponentModule,
    OrderHeaderComponentModule,
    OrderFooterComponentModule,
   ],
})
export class CreateOrderProductPageModule {}
