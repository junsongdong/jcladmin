import { WholesaleOrder } from './../../app/models/wholesale.order.model';
import { WatchJS } from 'melanke-watchjs';
import { OrderProvider } from './../../providers/order/order';
import { ProductForOrder } from './../../app/models/product.model';
import { ProductProvider } from './../../providers/product/product';
import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController,LoadingController,AlertController, NavParams } from 'ionic-angular';
import { Product } from '../../app/models/wholesale.order.model';
import { BarcodeScanner } from '@ionic-native/barcode-scanner'; 

@IonicPage()
@Component({
  selector: 'page-create-order-product',
  templateUrl: 'create-order-product.html',
})
export class CreateOrderProductPage {
  public pageNumber : string; 
  public headerTitle : string;  
  productCode : string;
  cProduct : ProductForOrder; 
  public cOrder : WholesaleOrder;
  productList :any;
  total : any;
  constructor(public navCtrl: NavController,
    public productProvider : ProductProvider,
    private loadingCtrl :LoadingController,
    private alertCtrl :AlertController ,
    private orderProvider : OrderProvider,
    private _zone: NgZone,
    private qrScanner: BarcodeScanner,
    public navParams: NavParams) {
    this.pageNumber = "2";
    this.headerTitle = "产品"
    this.productCode = "";
    this.productList =  [];
    this.total = 0; 
  }  

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateOrderProductPage');
  }
  ionViewCanLeave(){
    this.updateOrderProducts();
  }
  ionViewWillEnter(){
    this.showCamera();
 }
 ionViewWillLeave(){
    this.hideCamera(); 
 }
  ngOnInit(): void { 
    this.total = 0;
    this.cOrder = this.orderProvider.getCurrentOrder();
    this.loadProducts();
    this.calculTotalPrice(); 
    this.closeFirstLevel();
  }
  loadProducts(){
    let self = this;
    if(this.cOrder.products.length > 0){ 
        this.productList =  [];
        this.cOrder.products.forEach((item) => {
          this.productList.push(Object.assign({}, item)); 
       }) 
     this.productProvider.setProductListForOrder(this.productList);
    } 
  }
  searchProductByCode(){  
    let loading = this.loadingCtrl.create(); 
    loading.present();
    this.productProvider.getProductByCode(this.productCode).then((result) => {
      this.cProduct = new ProductForOrder(result);
      this.cProduct.productCode = this.productCode;
      this.productList.push(this.cProduct);
      this.productProvider.setProductListForOrder(this.productList);
      loading.dismiss();
      this.closeFirstLevel();
    }).catch((err) => {
      this.closeFirstLevel();
      loading.dismiss(); 
      let alert = this.alertCtrl.create({ 
        subTitle: "没有找到产品",
        buttons: ['OK']
      });
      alert.present(); 
    }); 
  }

  searchProductByQRcode(){ 
    this.qrScanner.scan().then(barcodeData => { 
      if(barcodeData.text != ""){
        this.productCode = barcodeData.text;
        this.searchProductByCode();
      }  
    }, (err) => {
      let alert = this.alertCtrl.create({ 
        subTitle: "扫描失败,请设置允许软件摄像权限",
        buttons: ['OK']
      });
      alert.present();
        console.log('Error: ', err);
    });
   // Optionally request the permission early
  /**  this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted  
          // start scanning
          let scanSub = this.qrScanner.scan().subscribe((text: string) => {
            console.log('Scanned something', text); 
           this.qrScanner.hide(); // hide camera preview
            scanSub.unsubscribe(); // stop scanning
            if(text != ""){
              this.productCode = text;
              this.searchProductByCode();
            } 
          });
          this.qrScanner.show();
        } else if (status.denied) {
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
          let alert = this.alertCtrl.create({ 
            subTitle: "请设置允许软件摄像权限",
            buttons: ['OK']
          });
          alert.present(); 
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
          let alert = this.alertCtrl.create({ 
            subTitle: "请设置允许软件摄像权限",
            buttons: ['OK']
          });
          alert.present(); 
        }
      })
      .catch((e: any) => console.log('Error is', e));**/
  }
  toggleSection(i) {
    this.productList[i].open = !this.productList[i].open;
    this.closeThirdLevel();
  }
  closeThirdLevel() {
    this.productList.forEach(element => {
      element.children.forEach(c => {
        c.open = false;
      });
   });
  }
  closeFirstLevel() {
    this.productList.forEach(element => {
      element.open = false;
   });
  }
  toggleItem(i, j) {
    this.productList[i].children[j].open = !this.productList[i].children[j].open;
  }
  deleteProduct(product){ 
    this.productProvider.deleteProductForOrder(this.productList,product);
    this.productProvider.setProductListForOrder(this.productList);
     // set current product list to order. 
    this.updateOrderProducts();
    this.calculTotalPrice(); 
  
  }
  negative(product,object){
    if(object && object.count && object.count >0){
      object.count = object.count -1;
    }
    this.getProductOrderedListByCode(product);
  }
  add(product,object){ 
    object.count = object.count + 1;
    this.getProductOrderedListByCode(product);
   }
   
  getProductOrderedListByCode(productForOrder : ProductForOrder){ 
    productForOrder.orderedList = []; 
    productForOrder.children.forEach((p) =>{
      if( p.unit == "piece"){
          p.children.forEach(color => {
            color["sizes"].forEach(size => { 
              let pCode =  productForOrder.productCode;
              let orderObj = {} ; 
              orderObj["productCode"] = pCode;
              orderObj["size"] = size.name;
              orderObj["sizeId"] = size.sizeId;
              orderObj["color"] = color["translation"]["zh-cn"];
              orderObj["colorNo"] = color["colorNo"];
              orderObj["count"] = size.count;
              orderObj["pieceCount"] = size.count;
              orderObj["unit"] = p.unit;
              if(orderObj["count"] > 0){
                productForOrder.orderedList.push(orderObj); 
              } 
            });
          });
       }
       if( p.unit == "bag" || p.unit == "box"){
          p.children.forEach(prop => { 
              let pCode =  prop["productCode"];
              let orderObj = {} ;
              orderObj["productCode"] = pCode;
              orderObj["unit"] = p.unit;
              orderObj["count"] = prop["count"];
              if(p.unit == "bag")
                orderObj["pieceCount"] = prop["count"]*prop["pieceInBag"];
              if(p.unit == "box")
                orderObj["pieceCount"] = prop["count"]* prop["totalPiece"];
              if(prop["count"] > 0)
              productForOrder.orderedList.push(orderObj);
          });
      } 
    }); 

    productForOrder.count =  this.calculProductForOrderCount(productForOrder);
    productForOrder.total = this.calculTotalPriceByProduct(productForOrder).replace(".",","); 
    this.calculTotalPrice();
  }
   // calcul total pieces selected
  calculProductForOrderCount(productForOrder){ 
    let count = 0;
    productForOrder.orderedList.forEach(order => {
      count =  order["pieceCount"] + count;
    });
    return count ;
  }
   // calcul total prices selected
  calculTotalPriceByProduct(productForOrder){  
    return    Number(productForOrder.count * productForOrder.price.value).toFixed(2); 
   
  } 
  onPiecePriceChange(productForOrder){
    productForOrder.total = this.calculTotalPriceByProduct(productForOrder).replace(".",","); 
    this.calculTotalPrice();
  }

  calculTotalPrice(){ 
    this.total = 0;
    if(this.productList.length > 0){
      this.productList.forEach(product => {
        this.total =  (Number.parseFloat(this.total) + Number.parseFloat(product.total)).toFixed(2) ;
      }); 
    } 
   // let products = this.productProvider.getProductListForOrder();
     this.orderProvider.getCurrentOrder().payment.gross = Number.parseFloat(this.total);
  }

  updateOrderProducts(){
    this.cOrder = this.orderProvider.getCurrentOrder();
    // set current product list to order. 
    this.cOrder.products =  [];
    if(this.productList && this.productList.length > 0){   
      this.productList.forEach((item) => {
        this.cOrder.products.push(Object.assign({}, item));
      }); 
    }
    this.orderProvider.setCurrentOrder(this.cOrder);
  }
  showCamera() {
    (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
  }
  
  hideCamera() {
    (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
  }
}
