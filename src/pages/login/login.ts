import { UserProvider } from './../../providers/user/user';
import { AuthProvider } from './../../providers/auth/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController,AlertController,NavParams } from 'ionic-angular'; 
import { Validators, FormBuilder, FormGroup,FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import * as CryptoJS from 'crypto-js'; 
import { User } from '../../app/models/user.model';
import { StorageProvider } from '../../providers/storage/storage';
import { App,Platform } from 'ionic-angular';
import { ConfigProvider } from '../../providers/config/config';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  username : any;
  password : any;
  group :  any; 
  groups : any;
  store : any;
  stores : any; 
  token: any;
  user : User;
  public form   : FormGroup; 
  loading ; any;
  configData : any;
  private onResumeSub:any; 
  constructor(public navCtrl: NavController, 
    public _FB : FormBuilder,
    public auth : AuthProvider,
    public app : App,
    public userProvider : UserProvider,
    public loadingCtrl : LoadingController,
    public alertCtrl: AlertController,
    public storage : Storage,
    public  storageProvider : StorageProvider,
    public  configProvider : ConfigProvider,
    public platform : Platform,
    public navParams: NavParams) {
    this.groups = [];
   /** this.groups =  [
      {
        serviceKey : "JCL",
        name   : "JCL GROUP"
      } 
    ]; **/
    
    /**this.stores =  [
      {
        value : "73436c17-c64f-426e-ae10-fc7d9b81e252",
        name   : "BY JULIE"
      },
      {
        value : "05c605a4-ac1f-42e4-8a76-8b8931ee37f0",
        name   : "JCL"
      },
      {
        value : "cc403259-ec4b-4bea-83c4-ec0791c03c23",
        name   : "zhanlai"
      }
    ]; **/
    // form validaton
    this.form = this._FB.group({
      f_username: ['', Validators.required],
      f_password: ['', Validators.required],
      group: ['' ],
      store: ['' ] 
    });


    this.onResumeSub = this.platform.resume.subscribe((event:any) => {
       
    });
      
  } 
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');  
    this.username = "xuanzhaopeng@gmail.com";
    this.password = "123456"; 
     // set default group
    // this.group = this.groups[0].serviceKey;  
      // set default group
    this.store = this.stores[0].value;  
  }
  ionViewDidEnter(){   
  }
  ngOnInit(): void {
    this.showLoading();
    this.stores = this.configProvider.getStores();
    this.configProvider.getAppData().then((data) => {
      if( this.loading)
      this.loading.dismiss();  
      let infoList = [] as any;
      infoList = data;
      if(data && infoList.length >2)
      this.configData = data[0];  
      this.groups = data[1].data; 
      if(this.groups && this.groups.length > 0){  
        // set default group
        this.group = this.groups[0].serviceKey;  
      }
    },
    (err) => {   
      if( this.loading)
      this.loading.dismiss();  
      console.log("getConfigDataRemote KO "); 
      let alert = this.alertCtrl.create({ 
        subTitle: '用户数据下载失败,请检查网络稍后再试',
        buttons: ['OK']
      });
      alert.present();
    }); 
  }
  ngOnDestroy(): void { 
    this.onResumeSub.unsubscribe(); 
  }
  login() {
    this.showLoading() 
    this.userProvider.generateToken(this.group,this.username,this.password); 
    this.auth.login().then((data: Response) => {
      if( this.loading)
      this.loading.dismiss();  
      this.user = new User(); 
      this.user.token =  this.userProvider.token; 
      this.user.username = data["lastName"]+ " " + data["firstName"];
      this.user.store = this.store;
      this.user.id = data['id'];
      this.userProvider.saveUser(this.user);  
      this.app.getActiveNav().setRoot('TabsPage'); 
    },
    (err) => {
      console.log(err);
      let msg =  "登录失败";
      this.showError(msg);
      if( this.loading)
      this.loading.dismiss();  
      this.userProvider.clearUser();
    }); 
  }
  
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: '请稍等...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
 
  showError(text) { 
    let alert = this.alertCtrl.create({
      title: '错误',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

  getAppData (){
    // get config data 
    this.configProvider.getConfigDataRemote().then(
      (result) => {
        console.log("getConfigDataRemote OK "); 
        this.configData = this.configProvider.configData;
         
      },
      (err) => {
     
        return false;
      }
    ); 
    // get group list
    this.configProvider.getGroupRemote().then(
      (result) => {
        console.log("getGroupRemote OK ");  
        this.groups =  this.configProvider.groups; 
        if(this.groups && this.groups.length > 0){  
          // set default group
          this.groups = this.groups[0].value;  
        }
      },
      (err) => {
        console.log("getGroupRemote KO "); 
        let alert = this.alertCtrl.create({ 
          subTitle: '用户数据下载失败,请检查网络稍后再试',
          buttons: ['OK']
        });
        alert.present();
      }
    );
   }
}
