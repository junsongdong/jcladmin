import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserInfoPage } from './user-info';
import { TranslateSharedModule } from '../../shared/translate-shared.module'; 
@NgModule({
  declarations: [
    UserInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(UserInfoPage),
    TranslateSharedModule
  ],
})
export class UserInfoPageModule {}
