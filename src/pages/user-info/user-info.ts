import { User } from './../../app/models/user.model';
import { UserProvider } from './../../providers/user/user';
import {  Component } from '@angular/core';
import { App ,IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { ConfigProvider } from '../../providers/config/config';

@IonicPage()
@Component({
  selector: 'page-user-info',
  templateUrl: 'user-info.html',
})
export class UserInfoPage {
  language : any;
  user : User;
  username : any;
  store : any;
  stores : any; 
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public userProvider : UserProvider,
              public configProvider : ConfigProvider,
              public storage : Storage,
              public app : App,
              public translateService : TranslateService) { 
              this.stores =  [ 
              ];
                this.language = "cn"; 
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad UserInfoPage');
   
  }
  
  ionViewWillEnter(){
    this.stores = this.configProvider.getStores();
    this.language = "cn"; 
    this.loadUserInfo();  
  }

  onLanguageChange(){
    this.translateService.use(this.language);
  }

  loadUserInfo(){
    let self =  this;
    this.storage.get('user').then((user) => {
      if(user != null){ 
        let userObj = JSON.parse(user);
        self.username = userObj.username; 
        self.store = userObj.store;
      }else{ 
      } 
    });  
  }
  logout(){
    this.userProvider.clearUser(); 
    this.app.getRootNav().setRoot("LoginPage");
 
  }
}
