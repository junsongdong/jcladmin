import { WholesaleOrder } from './../../app/models/wholesale.order.model';
import { Component } from '@angular/core';
import { IonicPage, NavController,LoadingController ,ModalController, NavParams, AlertController,ToastController } from 'ionic-angular';
import { WholesaleCompany, WholesaleCompanyAddress } from '../../app/models/wholesale.company.model';
import { NgForOf } from '@angular/common';
import { ClientProvider } from '../../providers/client/client';
import { Validators, FormBuilder, FormGroup,FormControl } from '@angular/forms'; 
import { Events } from 'ionic-angular';  
import { OrderProvider } from '../../providers/order/order';
 
@IonicPage()
@Component({
  selector: 'page-create-order-client',
  templateUrl: 'create-order-client.html',
})
export class CreateOrderClientPage {
  public pageNumber : string; 
  public headerTitle : string; 
  public cWholesaleCompany : WholesaleCompany; 
  public civilities  : any; 
  public types  : any;
  public taxTypes  : any;
  public isCreateMode : boolean;
  public displayClientForm : boolean; 
  public form   : FormGroup; 
  public isWholesaleCompanyChanged : boolean;
  public ignoreChangedCheck : boolean;
  public oldCompanyVal  : any;
  public noChange :  Function; 
  public isWholesaleCompanyChangedSub : any;
  public  isWholesaleCompanyNoChangedSub : any;
  public cOrder : WholesaleOrder;
  public loading : any;
  constructor(public navCtrl: NavController, 
    public modalCtrl: ModalController,
    public clientProvider :ClientProvider,
    public orderProvider : OrderProvider,
    public _FB : FormBuilder,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController, 
    public loadCtrl : LoadingController,
    public events: Events,
    public navParams: NavParams) {
    this.pageNumber = "1";
    this.headerTitle = "客户";
    this.civilities = [ 
        {
          value : "SARL",
          name   : "SARL"
        },
        {
          value : "SA",
          name   : "SA"
        },
        {
          value : "EURL",
          name   : "EURL"
        },
        {
          value : "Group",
          name   : "Group"
        } 
    ]
    
    this.types = [
      {
        value : "CA",
        name   : "CENTRAL D' ACHAT"
      },
      {
        value : "CM",
        name   : "Chaine de Magasin"
      },
      {
        value : "DC",
        name   : "DISCOUNTER"
      },
      {
        value : "GS",
        name   : "GRANDE MOYENNE SUR FACE"
      },
      {
        value : "GR",
        name   : "GROSSITE - IMPORTATEUR"
      },
      {
        value : "DT",
        name   : "DETAILLANT"
      },
      {
        value : "FO",
        name   : "Artisan"
      },
      {
        value : "DI",
        name   : "DISTRIBUTEUR"
      },
      {
        value : "FR",
        name   : "FRANCHISE"
      },
      {
        value : "LN",
        name   : "LISTE NOIRE"
      },
      {
        value : "CO",
        name   : "CONCURRENT"
      } 
      
    ]
    this.taxTypes = [

      {
        value : "FR",
        name   : "FR"
      },
      {
        value : "EXP",
        name   : "EXP"
      },
      {
        value : "CEE",
        name   : "CEE"
      },
      {
        value : "EXO",
        name   : "EXO"
      },
      {
        value : "DOM",
        name   : "DOM"
      } 
    ]
    this.cWholesaleCompany = new WholesaleCompany(); 
    this.displayClientForm = false;
    this.isWholesaleCompanyChanged = false;
    this.oldCompanyVal = {};
    this.noChange = this.setIgnoreChange.bind(this);
    this.ignoreChangedCheck = false;

    this.events.subscribe('wholesaleCompany:changed', ( ) => {  
      this.isWholesaleCompanyChanged = true;
    });
    this.events.subscribe('wholesaleCompany:nochanged', ( ) => {
      this.isWholesaleCompanyChanged = false;
    });
 
  }
  ionViewWillEnter() {
   
  }
 
  ionViewWillLeave() {
    this.clientProvider.setCurrentWholesaleCompany(this.cWholesaleCompany);
     
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateOrderClientPage'); 
   
  } 
  ngOnInit(): void { 
    let self = this;
    this.cOrder = this.orderProvider.getCurrentOrder();  
    var data = this.navParams.get('data');
    if(data && data == "create"){
      this.resetClientInfo();
      this.displayClientForm = false;
    }else{ 
      this.displayClientForm = false;
      this.cWholesaleCompany = this.clientProvider.cWholesaleCompany;
      if(this.cWholesaleCompany){ 
        let clientCode = this.cWholesaleCompany.code;
        if( clientCode && clientCode != "" ){
          this.updateClientAddressListStatus();
          this.isCreateMode  = false;
          this.displayClientForm = true;
        }else{ 
          this.resetClientInfo();
          this.isCreateMode  = true; 
        } 
      } 
    } 
  }
 ngOnDestroy(): void { 
  this.events.unsubscribe('wholesaleCompany:changed', null);
  this.events.unsubscribe('wholesaleCompany:nochanged', null);
 }
  ionViewCanLeave(){
    let self = this;
    // when click on home button
    if( this.ignoreChangedCheck){
      return true;
    }
    if(this.isWholesaleCompanyChanged){
      let alert = this.alertCtrl.create({ 
        title : '信息',
        subTitle: '保存信息修改?',
        buttons: [
          {
            text: '取消', 
            handler: () => {  
              self.isWholesaleCompanyChanged = false;
              Object.assign(self.cWholesaleCompany,self.oldCompanyVal); 
            }
          },
          {
            text: '确认', 
            handler: () => {  
              self.isWholesaleCompanyChanged = false;
              this.onSaveClientClick();
            }
          }  
        ]
      });
      alert.present();
      return false;
    } else if( this.isWholesaleCompanyChanged == false &&  (!this.displayClientForm || this.cWholesaleCompany.code == "")){
      let alert = this.alertCtrl.create({ 
        title : '信息',
        subTitle: '请添加客户或新建客户是否保存',
        buttons: [
          {
            text: 'OK', 
            handler: () => { 
              self.isWholesaleCompanyChanged = false;
              Object.assign(self.cWholesaleCompany,self.oldCompanyVal); 
            }
          }   
        ]
      });
      alert.present();
      return false;
    }else{
      // set current client to order. 
      this.cOrder = this.orderProvider.getCurrentOrder(); 
      this.cOrder.wholesaleCompany = this.cWholesaleCompany; 
      return true;
    }
    
  }
  editAddress(address){
    
    let addAddressModal = this.modalCtrl.create('ClientAddressPage', { address : address });
    addAddressModal.onDidDismiss(data => { 
      if(data){
        this.cWholesaleCompany = this.clientProvider.cWholesaleCompany;
        this.updateClientAddressListStatus(); 
      }else{
        console.log("user cancel to edit address");
      }
    
    });
    addAddressModal.present();
  }
  
  goToSearchClientPage(){
    this.presentSearchClientModal();
  }
  presentAddClientModal() {
    let addClientModal = this.modalCtrl.create('AddClientPage');
    addClientModal.present();
  }
  presentSearchClientModal() {
    let self = this; 
    let searchClientModal = this.modalCtrl.create('SearchClientPage');
    searchClientModal.onDidDismiss(data => {
      if(data){
        this.resetClientInfo();
        this.isCreateMode = false;
        this.displayClientForm = true;
        //this.cWholesaleCompany = data; 
        Object.assign(self.oldCompanyVal,data); 
        Object.assign(this.cWholesaleCompany,data); 
        this.updateClientAddressListStatus();
        this.isWholesaleCompanyChanged = false; 
        // set current client to order.
        //self.cOrder = this.orderProvider.getCurrentOrder();  
        //self.cOrder.wholesaleCompany = self.cWholesaleCompany;
      } 
    }); 
    searchClientModal.present().then(
      data => { 
      }
    );
  }
  updateClientAddressListStatus(){  
    let addressList =  this.cWholesaleCompany.address;
    let billingAddress = this.cWholesaleCompany.billingAddress;
    let deliveryAddress = this.cWholesaleCompany.deliveryAddress;
    let ifBillingAddressSelected = false;
    let ifDeliveryAddressSelected = false;
     if( addressList && addressList.length > 0){  
       addressList.forEach( address => {  
        if( address.id  ==  this.cWholesaleCompany.billingAddress){
          address.ifBillingAddress = true;
        }else{
          address.ifBillingAddress = false;
        } 
        if( address.id  ==  this.cWholesaleCompany.deliveryAddress){
          address.ifDeliveryAddress = true;
        }else{
          address.ifDeliveryAddress = false;
        }
      });  
    }  
  }  
  deleteClientAddress(client){
    let addressList =  this.cWholesaleCompany.address;
    let addressIndex = addressList.findIndex(item => item.id == client.id);
    addressList.splice(addressIndex, 1); 
    this.isWholesaleCompanyChanged = true; 
  }
  updateDeliveryAddress(address : WholesaleCompanyAddress){
    
    if(address.ifDeliveryAddress == true){  
      this.cWholesaleCompany.deliveryAddress = address.id;
    }else{
      this.cWholesaleCompany.deliveryAddress = "";
    } 
    this.isWholesaleCompanyChanged = true; 
    this.updateClientAddressListStatus();
   
  }
  updateBillingAddress(address : WholesaleCompanyAddress){
    let addressList =  this.cWholesaleCompany.address; 
    if(address.ifBillingAddress == true){  
      this.cWholesaleCompany.billingAddress = address.id;
    }else{
      this.cWholesaleCompany.billingAddress = "";
    }   
    this.isWholesaleCompanyChanged = true; 
    this.updateClientAddressListStatus();
  }
  
  resetClientInfo(){
    this.cWholesaleCompany = new WholesaleCompany(); 
    this.cWholesaleCompany.code = "";
    this.cWholesaleCompany.civility = this.civilities[0]['value'];
    this.cWholesaleCompany.email = ""; 
    this.cWholesaleCompany.name = "";
    this.cWholesaleCompany.type = this.types[0]['value'];
    this.cWholesaleCompany.billingAddress = "";
    this.cWholesaleCompany.deliveryAddress = "";
    this.cWholesaleCompany.taxType = this.taxTypes[0]['value'];
    this.cWholesaleCompany.siret = "";
    this.cWholesaleCompany.tvaNumber = "";
    this.cWholesaleCompany.telephone = "";
    this.cWholesaleCompany.address = [];
    this.cWholesaleCompany.riskCodeId = "";
    this.clientProvider.setCurrentWholesaleCompany(this.cWholesaleCompany);
    this.isWholesaleCompanyChanged = false;
  }
  
  onAddClientClick(){
    this.displayClientForm = true;
    this.isCreateMode  = true;
    this.resetClientInfo();
  }

  onSaveClientClick(){
    let self = this;
    this.clientProvider.setCurrentWholesaleCompany(this.cWholesaleCompany); 
    if( this.cWholesaleCompany.name == ""){
      let alert = this.alertCtrl.create({ 
        title : '错误',
        subTitle: '请输入企业名',
        buttons: ['OK']
      });
      alert.present();
      return;
    }
    if( this.cWholesaleCompany.address.length == 0){
      let alert = this.alertCtrl.create({ 
        title : '错误',
        subTitle: '请添加企业地址',
        buttons: ['OK']
      });
      alert.present();
      return;
    } 
    if(this.isCreateMode){
      this.clientProvider.sendClientInfo('create').then(
        (result) => {  
           // set current client to order.
          self.cOrder.wholesaleCompany = this.cWholesaleCompany;
          self.isWholesaleCompanyChanged = false;
           this.isCreateMode = false;
           Object.assign(this.cWholesaleCompany , result); 
           Object.assign(self.oldCompanyVal,result); 
           this.updateClientAddressListStatus();
           let toast = this.toastCtrl.create({
            message: '客户账户创建成功',
            duration: 3000,
            position: 'bottom'
          }); 
          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
          }); 
          toast.present();
        }, 
        (err) => {
          let alert = this.alertCtrl.create({ 
            title : '错误',
            subTitle: '创建客户账户失败',
            buttons: ['OK']
          });
          alert.present();
        }); 
    } else{
      this.clientProvider.sendClientInfo('update').then(
        (result ) => {
          this.isCreateMode = false;   
          self.isWholesaleCompanyChanged = false;
          Object.assign(this.cWholesaleCompany , result); 
          this.updateClientAddressListStatus();
          let toast = this.toastCtrl.create({
            message: '客户账户更新成功',
            duration: 3000,
            position: 'bottom'
          }); 
          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
          }); 
          toast.present();
        },
        (err) => {
          let alert = this.alertCtrl.create({ 
            title : '错误',
            subTitle: '更新客户账户失败',
            buttons: ['OK']
          });
          alert.present();
        }); 
    }  
  } 
 
  setIgnoreChange (){ 
    this.ignoreChangedCheck = true;
  }
  onCompanyInfoChange(){
    this.isWholesaleCompanyChanged = true;
  }

  verifyTva(){
    if(this.cWholesaleCompany.tvaNumber != ""){
      this.clientProvider.tvaValidate(this.cWholesaleCompany.tvaNumber).then(
        (result ) => {
         this.displayTAVDetail(result);
        },
        (err) => {
          let alert = this.alertCtrl.create({ 
            title : '',
            subTitle: 'TVA验证失败,请检查网络状况稍后再试',
            buttons: ['OK']
          });
          alert.present();
        });  
    }else{
      let alert = this.alertCtrl.create({ 
        title : '',
        subTitle: '请输入TVA',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  
  displayTAVDetail(data){ 
      let self = this;
      let modal = this.modalCtrl.create('TvaPage', { tva : data });
       modal.onDidDismiss(data => { 
      }); 
      modal.present(); 
  }
  showLoading() {
    this.loading = this.loadCtrl.create({ 
      dismissOnPageChange: true
    });
    this.loading.present();
  }
  hdieLoading() {
    if(this.loading)
     this.loading.dissmiss();  
    
  }
}
