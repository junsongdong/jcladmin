import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateOrderClientPage } from './create-order-client';
import { BreadcrumbsComponentModule } from '../../components/breadcrumbs/breadcrumbs.module';
import { OrderHeaderComponentModule } from '../../components/order-header/order-header.module';
import { OrderFooterComponentModule } from '../../components/order-footer/order-footer.module';
  
@NgModule({
  declarations: [
    CreateOrderClientPage  
  ],
  imports: [  
    IonicPageModule.forChild(CreateOrderClientPage),
    BreadcrumbsComponentModule,
    OrderHeaderComponentModule,
    OrderFooterComponentModule
  ],
})
export class CreateOrderClientPageModule {}
