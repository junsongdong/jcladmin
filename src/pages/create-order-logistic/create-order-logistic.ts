import { OrderProvider } from './../../providers/order/order';
import { WholesaleOrder } from './../../app/models/wholesale.order.model';
import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, ModalController,AlertController,NavParams } from 'ionic-angular';
import { LogisticProvider } from '../../providers/logistic/logistic';
import { SelectSearchableComponent } from 'ionic-select-searchable';

@IonicPage()
@Component({
  selector: 'page-create-order-logistic',
  templateUrl: 'create-order-logistic.html',
})

export class CreateOrderLogisticPage { 
  public pageNumber : string; 
  public headerTitle : string; 
  paymentMethordList : any ; 
  transportMethordList : any ; 
  transportCompanyList : any ;  
  p : any;
  pSize : any;
  ports: any;
  port: any;
  public cOrder : WholesaleOrder;
  public onSubmit: Function; 
  constructor(public navCtrl: NavController, 
    public orderProvider : OrderProvider,
    public logisticProvider : LogisticProvider,
    public modalCtrl : ModalController,
    public alertCtrl : AlertController,
    public navParams: NavParams) {
    this.pageNumber = "4";
    this.headerTitle = "物流";
    this.p = 1;
    this.pSize = 500;
    this.paymentMethordList = [
      {
        id : "1",
        name   : "Port dû"
      },
      {
        id : "2",
        name   : "Port payé"
      }
    ];
  
   /** this.transportCompanyList = [
      {
        id : "012c550c-9224-4d49-9f9a-abd3f32d9a89",
        name   : "翰威"
      },
      {
        id : "1724b6cc-6b8f-476c-b191-004ec534a955",
        name   : "嘉宏"
      },
      {
        id : "23f36be1-63f6-4c65-9b4c-b87407aae8cf",
        name   : "瑞创"
      },
      {
        id : "2df437bf-c651-4c7b-94cf-a78085f2f874",
        name   : "asdfwewe"
      },
      {
        id : "50c89da8-68ee-420f-856d-d524357bda92",
        name   : "ewewe"
      },
      {
        id : "dce9512f-8eb3-4218-82f3-fd3878fb9a46",
        name   : "丹马士"
      },
      {
        id : "f0c3b922-dc34-4c3c-8927-e2deaec96930",
        name   : "江海"
      }
    ];**/
    this.transportMethordList = [
      {
        id : "1",
        name   : "Envoyer nous même"
      },
      {
        id : "2",
        name   : "Client prend lui même au magazin"
      },
      {
        id : "3",
        name   : "Client prend lui même au dépôt"
      },
      {
        id : "4",
        name   : "La poste"
      },
      {
        id : "5",
        name   : "EXAPAQ"
      },
      {
        id : "6",
        name   : "Société de transport spécifiée"
      },

    ];

    this.onSubmit =  this.onOrderSubmit.bind(this);
  }  
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateOrderLogisticPage'); 
   
  }
  ngOnInit(): void { 
    this.cOrder = this.orderProvider.getCurrentOrder(); 
    this.logisticProvider.getTransportCompanies(this.p,this.pSize).then(
      (result) => {
        this.transportCompanyList = result;
        if(this.transportCompanyList && this.transportCompanyList.length > 0){
          if(!this.cOrder.transportCompany.id || this.cOrder.transportCompany.id == ""){
            let id =  this.transportCompanyList[0].id ? this.transportCompanyList[0].id : "";
            let name =  this.transportCompanyList[0].name ? this.transportCompanyList[0].name : "";
            let telephone =  this.transportCompanyList[0].telephone ? this.transportCompanyList[0].telephone : "";
            let fax =  this.transportCompanyList[0].fax ? this.transportCompanyList[0].fax : "";
            let address =  this.transportCompanyList[0].address ? this.transportCompanyList[0].address : "";
            this.cOrder.transportCompany.id = id;
           /** this.cOrder.transportCompany.name =  name;
            this.cOrder.transportCompany.telephone = telephone;
            this.cOrder.transportCompany.fax = fax;
            this.cOrder.transportCompany.address = address;*/
          } 
        } 
      },
      (err) => {
        console.log('getTransportCompanies error : ' + err);
      });
    if( !this.cOrder.transportMethod.id || this.cOrder.transportMethod.id == "")   
       this.cOrder.transportMethod.id =  this.transportMethordList[0].id;
    if( !this.cOrder.transportPaymentStatus.id || this.cOrder.transportPaymentStatus.id == "")
    this.cOrder.transportPaymentStatus.id =  this.paymentMethordList[0].id;
 
  } 
  searchTransportCompany(event: {
    component: SelectSearchableComponent,
    value: any 
    }) {
        console.log('port:', event.value);
    } 
  onOrderSubmit(){
       let self = this; 
       this.orderProvider.submitOrder(this.cOrder).then(
        (result) => { 
          let alert = this.alertCtrl.create({ 
            title : '信息',
            subTitle: '创建订单成功',
            buttons: [
              {
                text: 'OK', 
                handler: () => { 
                  setTimeout(
                    function(){ 
                      self.navCtrl.setRoot('TabsPage');
                      self.navCtrl.popToRoot({ animate: false });
                    },50
                  ); 
                }
              }  
            ]
          });
          alert.present();
        },
    (err) => {
         let alert = this.alertCtrl.create({ 
          title : '错误',
          subTitle: '创建订单失败',
          buttons: ['OK']
        });
        alert.present();
        }
      ); 
    }
}
 
