import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateOrderLogisticPage } from './create-order-logistic';
import { BreadcrumbsComponentModule } from '../../components/breadcrumbs/breadcrumbs.module';
import { OrderHeaderComponentModule } from '../../components/order-header/order-header.module';
import { OrderFooterComponentModule } from '../../components/order-footer/order-footer.module';
import {IonicModule} from 'ionic-angular';
import { SelectSearchableModule } from 'ionic-select-searchable';
 
@NgModule({
  declarations: [
    CreateOrderLogisticPage, 
  
  ],
  imports: [
    IonicPageModule.forChild(CreateOrderLogisticPage),
    IonicModule,
    BreadcrumbsComponentModule,
    OrderHeaderComponentModule,
    OrderFooterComponentModule,
    SelectSearchableModule
  ] 
})
export class CreateOrderLogisticPageModule {}
