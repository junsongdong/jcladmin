import { Component } from '@angular/core';  
import { IonicPage  } from 'ionic-angular'; 
@IonicPage()
@Component({
  selector: 'tabs-page',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'OrderSentPage';
  tab2Root = 'OrderDraftPage';
  tab3Root = 'UserInfoPage';

  constructor() {
 
  }
}
