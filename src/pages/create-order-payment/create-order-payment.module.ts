import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateOrderPaymentPage } from './create-order-payment';
import { BreadcrumbsComponentModule } from '../../components/breadcrumbs/breadcrumbs.module';
import { OrderHeaderComponentModule } from '../../components/order-header/order-header.module';
import { OrderFooterComponentModule } from '../../components/order-footer/order-footer.module';

@NgModule({
  declarations: [
    CreateOrderPaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateOrderPaymentPage),
    BreadcrumbsComponentModule,
    OrderHeaderComponentModule,
    OrderFooterComponentModule
  ],
})
export class CreateOrderPaymentPageModule {}
