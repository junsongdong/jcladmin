import { ProductProvider } from './../../providers/product/product';
import { ConfigProvider } from './../../providers/config/config';
import { OrderProvider } from './../../providers/order/order';
import { Payment, WholesaleOrder, PaymentRegulation, Deposit, PaymentType } from './../../app/models/wholesale.order.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-create-order-payment',
  templateUrl: 'create-order-payment.html',
})
export class CreateOrderPaymentPage {
  public pageNumber : string; 
  public headerTitle : string; 
  public cPayment : Payment; 
  public cOrder : WholesaleOrder;
  public paymentType : any;
  public paymentTypes : any; 
  public currencyType = "€" ; 
  public totalDiposits : any;
  public totalRegulations : any;
  constructor(public navCtrl: NavController,
     public orderProvider : OrderProvider,
     public configProvider : ConfigProvider, 
     public productProvider :ProductProvider,
     public navParams: NavParams) {
     this.pageNumber = "3";
     this.headerTitle = "支付"  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateOrderPaymentPage');
  }
  ngOnInit(): void {
     this.paymentTypes =  this.configProvider.configData.paymentTypes; 
     this.cOrder = this.orderProvider.getCurrentOrder();
    // this.cOrder.payment.gross = 1000;  
     this.calcul("global");
     this.loadOfferInfo();
  }

  calcul(type){
    let gross = this.cOrder.payment.gross;
    this.cOrder.payment.discount  = Number( this.cOrder.payment.discount ? this.cOrder.payment.discount  : 0) ; 
    this.cOrder.payment.net =  Number(this.cOrder.payment.net ? this.cOrder.payment.net  : 0 ); 
    this.cOrder.payment.total =  Number(this.cOrder.payment.total ? this.cOrder.payment.total  : 0) ;  
    this.cOrder.payment.tax = Number(this.cOrder.payment.tax ? this.cOrder.payment.tax  : 0) ; 
    this.cOrder.payment.transportCost =  Number( this.cOrder.payment.transportCost ? this.cOrder.payment.transportCost  : 0 ); 
    this.cOrder.payment.discountPercentage = Number( this.cOrder.payment.discountPercentage ? this.cOrder.payment.discountPercentage  : 0) ; 
    this.cOrder.payment.paymentDiscount = Number(this.cOrder.payment.paymentDiscount ? this.cOrder.payment.paymentDiscount  : 0) ; 
   
    // if( gross != 0){ 
      if(type == "discountPercentage"){ 
        this.cOrder.payment.discount =  Number((this.cOrder.payment.discountPercentage *this.cOrder.payment.gross/100).toFixed(2));
      }else{
        this.cOrder.payment.discountPercentage  = Number((((this.cOrder.payment.discount/this.cOrder.payment.gross)*100)).toFixed(2));
      } 
      if( gross == 0){ 
        this.cOrder.payment.discountPercentage = 0;
      }
      this.cOrder.payment.net = Number((this.cOrder.payment.gross -  this.cOrder.payment.discount - (this.cOrder.payment.paymentDiscount/100)*gross).toFixed(2));
      this.cOrder.payment.tax = Number((this.cOrder.payment.net*0.2).toFixed(2));
      this.cOrder.payment.total = Number((this.cOrder.payment.net - this.cOrder.payment.tax  -  this.cOrder.payment.transportCost).toFixed(2));
      this.calculRestToPay(); 
    // }  
  }
  loadOfferInfo() {
    if (this.cOrder != null) { 
      // if no payment regulations , create a new empty one
      if (this.cOrder.payment.paymentRegulations == null || this.cOrder.payment.paymentRegulations.length == 0) { 
        this.addNewPaymentInputField();
        this.totalRegulations = 0;
      }  
      // if no payment Diposit , create a new empty one
      if (this.cOrder.payment.deposits == null || this.cOrder.payment.deposits.length == 0) { 
        this.addNewDepositInputField();
        this.totalDiposits = 0;
      } 
      
    } 
  }
  addNewPaymentInputField(): void {
    let newPRegulation = new PaymentRegulation();
    newPRegulation.id = "infodo_" + Date.now();
    newPRegulation.payDate = moment().toDate().toString(); 
    newPRegulation.paymentType = new PaymentType();
    if(this.paymentTypes && this.paymentTypes.length > 0)
    newPRegulation.paymentType.id = this.paymentTypes[0].id;  
    newPRegulation.value = 0;
    newPRegulation.currency= this.currencyType;  
    this.cOrder.payment.paymentRegulations.push(newPRegulation);
    
  }
  removePaymentInputField(obj): void {
    let removeInfoDoObjIndex =  this.cOrder.payment.paymentRegulations.findIndex(item => item.id == obj.id);
    this.cOrder.payment.paymentRegulations.splice(removeInfoDoObjIndex, 1); 
    this.calculRestToPay(); 
  }
  addNewDepositInputField(): void {
    let newDiposit = new Deposit();
    newDiposit.id = "infodo_" + Date.now();
    newDiposit.payDate = moment().toDate().toString();
    newDiposit.paymentType = new PaymentType();
    if(this.paymentTypes && this.paymentTypes.length > 0)
    newDiposit.paymentType.id= this.paymentTypes[0].id; 
    newDiposit.value = 0;
    newDiposit.currency= this.currencyType; 
    this.cOrder.payment.deposits.push(newDiposit);
    
  }
  removeDepositInputField(obj): void {
    let removeInfoDoObjIndex =  this.cOrder.payment.deposits.findIndex(item => item.id == obj.id);
    this.cOrder.payment.deposits.splice(removeInfoDoObjIndex, 1);  
    this.calculRestToPay();
  } 
  calculRegulations(){
    this.totalRegulations = 0;
    this.cOrder.payment.paymentRegulations.forEach((regulation) =>{
      regulation.value =  Number(regulation.value);
      this.totalRegulations =   Number(this.totalRegulations) +  regulation.value ; 
    });
  }
  calculDiposits(){
    this.totalDiposits = 0;
    this.cOrder.payment.deposits.forEach((deposit) =>{
      deposit.value = Number(deposit.value);
      this.totalDiposits =  Number(this.totalDiposits) + deposit.value; 
    });
  }
  calculRestToPay(){
    this.calculRegulations();
    this.calculDiposits(); 
    this.cOrder.payment.restToPay =  this.cOrder.payment.total - this.totalRegulations - this.totalDiposits ;
    this.cOrder.payment.restToPay =  Number( this.cOrder.payment.restToPay.toFixed(2));
  }
  exchangeValue(regulation){  
    let restToPay = this.cOrder.payment.restToPay;
    let regulationVal = regulation.value;  
      this.cOrder.payment.restToPay = regulationVal ;
      regulation.value = restToPay; 
  }
}
