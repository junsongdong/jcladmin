import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientAddressPage } from './client-address';

@NgModule({
  declarations: [
    ClientAddressPage,
  ],
  imports: [
    IonicPageModule.forChild(ClientAddressPage),
  ],
})
export class ClientAddressPageModule {}
