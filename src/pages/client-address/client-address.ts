import { ConfigProvider } from './../../providers/config/config';
import { Component } from '@angular/core';
import { IonicPage, NavController,ViewController,AlertController, NavParams } from 'ionic-angular'; 
import { WholesaleCompanyAddress, WholesaleCompany } from '../../app/models/wholesale.company.model';
import { ClientProvider } from '../../providers/client/client';
import { OrderProvider } from '../../providers/order/order';
 
@IonicPage()
@Component({
  selector: 'page-client-address',
  templateUrl: 'client-address.html',
})
export class ClientAddressPage {
  public cClientAddress : WholesaleCompanyAddress;
  public cWholesaleCompany : WholesaleCompany;  
  public isCreateMode : boolean;
  public countries : any;
  public isDeliveryAddressDisabled : boolean; 
  public isBillingAddressDisabled : boolean; 
  constructor(public navCtrl: NavController, 
    public viewCtrl: ViewController, 
    public alertCtrl : AlertController,
    public clientProvider : ClientProvider,
    public configProvider : ConfigProvider,
    public orderProvider : OrderProvider,
    public navParams: NavParams) {
     
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientAddressPage');
  }
  ngOnInit(): void {   
    this.getCountryList();
    this.cClientAddress = this.navParams.get("address");
    this.cWholesaleCompany = this.clientProvider.cWholesaleCompany;  
    // if update
    if(this.cClientAddress){
      this.isCreateMode  = false;  
    }else{
      // if create
      this.isCreateMode  = true; 
      this.resetAddressInfo(); 
    }
    this.updateAddressStatus();
  }
  close(){
     this.viewCtrl.dismiss( );
  }
  updateAddressStatus(){ 
    let addressList =  this.cWholesaleCompany.address;
    let billingAddress = this.cWholesaleCompany.billingAddress;
    let deliveryAddress = this.cWholesaleCompany.deliveryAddress; 
    if(this.isCreateMode){
      if(addressList.length == 0){
       this.isDeliveryAddressDisabled = true;
       this.isBillingAddressDisabled = true;
       this.cClientAddress.ifDeliveryAddress =  true
       this.cClientAddress.ifBillingAddress = true
       this.cWholesaleCompany.billingAddress =  this.cClientAddress.id;
       this.cWholesaleCompany.deliveryAddress =  this.cClientAddress.id;
      } 
    } else{
      if(addressList.length == 1){
        this.isDeliveryAddressDisabled = true;
        this.isBillingAddressDisabled = true;
        this.cClientAddress.ifDeliveryAddress =  true
        this.cClientAddress.ifBillingAddress = true
        this.cWholesaleCompany.billingAddress =  this.cClientAddress.id;
        this.cWholesaleCompany.deliveryAddress =  this.cClientAddress.id;
       } 
    }  
  } 
 
  onDeliveryAddressChange(){   
    if( this.cClientAddress.ifDeliveryAddress == true){ 
      this.cWholesaleCompany.deliveryAddress =  this.cClientAddress.id;
    } 
  }
  onBillingAddressChange(){ 
    if( this.cClientAddress.ifBillingAddress == true){
      this.cWholesaleCompany.billingAddress =  this.cClientAddress.id;
    }  
  }  
  resetAddressInfo(){
    this.cClientAddress = new WholesaleCompanyAddress(); 
    this.cClientAddress.id = this.generateClientId();
    this.cClientAddress.telephone = "";
    this.cClientAddress.receiverName = ""; 
    this.cClientAddress.postalCode = ""; 
    this.cClientAddress.city = "";
    this.cClientAddress.address2 = "";
    this.cClientAddress.address1 = ""; 
    this.cClientAddress.countryName = ""; 
    this.cClientAddress.ifBillingAddress = false; 
    this.cClientAddress.ifDeliveryAddress = false; 
    if(this.countries && this.countries.length > 0)
    this.cClientAddress.countryCode = this.countries[0].code;
  }
   
  getCountryList(){
    // get config data  
    this.countries = this.configProvider.configData.countries; 
 }

 getCountryNameByCode(code): any{

  let country = this.countries.find(i => i.code === code); 
  return country.name;
 }
  saveAddress(){
    let countryName = this.getCountryNameByCode(this.cClientAddress.countryCode);
    this.cClientAddress.countryName = countryName;
    let addressList =  this.cWholesaleCompany.address;
    if(this.isCreateMode){
      this.cWholesaleCompany.address.push(this.cClientAddress);
    }else{
      let addressIndex = addressList.findIndex(item => item.id == this.cClientAddress.id);
      addressList.splice(addressIndex, 1);
      this.cWholesaleCompany.address.push(this.cClientAddress);
    }  

    this.clientProvider.setCurrentWholesaleCompany(this.cWholesaleCompany);
    this.viewCtrl.dismiss( {data : "update"});
  }
  getRandomInt( min, max ) {
    return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
 }
  generateClientId(){
    var tokens = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        chars = 5,
        segments = 4,
        keyString = "";
        
      for( var i = 0; i < segments; i++ ) {
        var segment = "";
        
        for( var j = 0; j < chars; j++ ) {
            var k = this.getRandomInt( 0, 35 );
          segment += tokens[ k ];
        }
        
        keyString += segment;
        
        if( i < ( segments - 1 ) ) {
          keyString += "-";
        }
      }
      
      return keyString; 
  }
   
  

}
 