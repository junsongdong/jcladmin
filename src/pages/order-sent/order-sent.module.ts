import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderSentPage } from './order-sent';

@NgModule({
  declarations: [
    OrderSentPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderSentPage),
  ],
})
export class OrderSentPageModule {}
