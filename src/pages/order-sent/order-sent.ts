import { Component } from '@angular/core';
import { IonicPage, NavController,LoadingController,AlertController, NavParams, App } from 'ionic-angular';
import { OrderProvider } from '../../providers/order/order';
import { ConfigProvider } from '../../providers/config/config';

@IonicPage()
@Component({
  selector: 'page-order-sent',
  templateUrl: 'order-sent.html',
})
export class OrderSentPage {
  loading :any;
  constructor(public navCtrl: NavController,
     public orderProvider :  OrderProvider, 
     public  configProvider : ConfigProvider,
     public loadingCtrl : LoadingController, 
     public alertCtrl : AlertController, 
     public app : App, 
     public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderSentPage');
  }
  ionViewWillEnter(){
    //this.showLoading();
    this.configProvider.getAppData().then((data) => {
      if( this.loading)
      this.loading.dismiss();   
    },
    (err) => {   
     // if( this.loading)
     // this.loading.dismiss();  
      console.log("getConfigDataRemote KO "); 
      let alert = this.alertCtrl.create({ 
        subTitle: '用户数据下载失败,请检查网络稍后再试',
        buttons: ['OK']
      });
      alert.present();
    }); 
  }
  goToCreateOrderPage(){
    this.orderProvider.initOrder();
    this.app.getRootNav().push('CreateOrderClientPage',{data :'create' },{ animate: false });
  }
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: '请稍等...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
 
  showError(text) { 
    let alert = this.alertCtrl.create({
      title: '错误',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }
}
