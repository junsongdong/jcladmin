import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams, LoadingController, AlertController, Searchbar } from 'ionic-angular';
import { ClientProvider } from '../../providers/client/client'; 
import {  Keyboard } from 'ionic-angular';

/**
 * Generated class for the SearchClientPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-client',
  templateUrl: 'search-client.html',
})
export class SearchClientPage {
  clients : any;
  page = 1;
  pSize = 20;
  keywords : any;
  total : any;  
  searching: any = false;  

  @ViewChild('searchInput') searchInput : Searchbar;; 
  constructor(public navCtrl: NavController,
     public viewCtrl : ViewController ,
     public alertCtrl : AlertController, 
     public clientProvider : ClientProvider,
     public keyboard : Keyboard,
     public navParams: NavParams) {
  } 
  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchClientPage'); 
    let self = this;
    setTimeout(() => {
      self.searchInput.setFocus(); 
    },500); 
  }
  ionViewDidEnter() {
  
  }
  ngOnInit(): void { 
   
  }
  close(){
    this.viewCtrl.dismiss();
  }
  searchClients(){ 
    if(this.keywords != ""){
        this.searching = true; 
      this.clientProvider.searchClients(this.keywords,this.page,this.pSize).then((result) => { 
        this.clients = result['list']; 
        this.searching = false; 
      },
      (err) => {
        this.searching = false; 
        this.showError("搜索客户失败,请检查网络稍后再试");
      });
    }
   } 
   onItemClicked(client){  
    this.viewCtrl.dismiss(client);
   }  
   showError(text) { 
    let alert = this.alertCtrl.create({
      title: '错误',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }
}
