import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderDraftPage } from './order-draft';

@NgModule({
  declarations: [
    OrderDraftPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderDraftPage),
  ],
  exports: [
    OrderDraftPage
  ]
})
export class OrderDraftPageModule {}
