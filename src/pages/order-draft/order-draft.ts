import { Component } from '@angular/core';
import { App,IonicPage, NavController, NavParams, NavPush } from 'ionic-angular';  
import { OrderProvider } from '../../providers/order/order';
 
@IonicPage()
@Component({
  selector: 'page-order-draft',
  templateUrl: 'order-draft.html',
})
export class OrderDraftPage {
  
  constructor(public navCtrl: NavController, 
    public orderProvider :  OrderProvider,
    public app : App,
    public navParams: NavParams) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderDraftPage');
  } 
  ionViewWillEnter(){
  
  }
  goToCreateOrderPage(){
    this.orderProvider.initOrder(); 
    this.app.getRootNav().push('CreateOrderClientPage',{data :'create' },{ animate: false });
  }



}
