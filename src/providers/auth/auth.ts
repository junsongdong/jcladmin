import { HttpClient ,HttpHeaders} from '@angular/common/http'; 
import { Injectable } from '@angular/core';
import { endpoints as ENV } from '../../app/environment';
import * as CryptoJS from 'crypto-js'; 
import { Observable } from 'rxjs/Observable';
import {Http, RequestOptionsArgs, Response, RequestOptions, ConnectionBackend, Headers} from '@angular/http';
import { User } from '../../app/models/user.model';
 
@Injectable()
export class AuthProvider { 
   user : User; 
  constructor(public http: HttpClient) { 
   
  }
  
  public login() {   
    return new Promise((resolve, reject) => { 
          this.http.get(ENV.BASEURL + '/admin/user')
         .subscribe(
          data =>  resolve(data),
          error => reject(error)
        );
    });
  } 
 
}
