import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';  
import 'rxjs/add/operator/mergeMap'; 
import { UserProvider } from '../user/user';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public userProvider: UserProvider) {
  
  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      let self = this;
      return self.userProvider.getUserObservable()
      .mergeMap((user) => {  
        let token =  (user != null) ?  JSON.parse(user).token : self.userProvider.token; 
        // clone and modify the request
        request = request.clone({
            setHeaders: {
                Authorization: `Basic ${token}`
            }
        });
        return next.handle(request);
      });
  } 
    
}
 