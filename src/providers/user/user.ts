 
import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js'; 
import { User } from '../../app/models/user.model';
import { Storage } from '@ionic/storage'; 
import 'rxjs/add/observable/fromPromise';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class UserProvider {
   public token : any;
   public user : any;
  constructor( 
    public storage : Storage) {
    console.log('Hello UserProvider Provider');
  }

  generateToken(group,username,password){ 
    password = password.toLowerCase();
    password = CryptoJS.MD5(password).toString();   
    this.token = btoa(group + "$" +username + ":" + password) 
  }
  saveUser(user : User){
    this.storage.set("user",JSON.stringify(user));
  } 
 
  getUserObservable (): Observable<string> {

    return Observable.fromPromise(this.getUser());
  } 
  getUser (): Promise<string> {
    return this.storage.get('user');  
  }
  
  clearUser(){
    this.storage.remove("user");
    this.user = null;
  }
}
