import { ProductProvider } from './../product/product';
import { WholesaleOrder, WholesaleSeller, DeliveryAddress, Product, Payment, TransportPaymentStatus, TransportMethod, TransportCompany, Preparation, InvoiceAddress } from './../../app/models/wholesale.order.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { WholesaleCompany } from '../../app/models/wholesale.company.model';
import { endpoints as ENV } from '../../app/environment';
import { WholesaleOrderSubmit } from '../../app/models/wholesale.ordersubmit.model';
import { Storage } from '@ionic/storage'; 

@Injectable()
export class OrderProvider {
  public cOrder : WholesaleOrder 
  constructor(public http: HttpClient,
    public productProvider:ProductProvider,
    public  storage : Storage) 
  {
    console.log('Hello OrderProvider Provider');
  }
  initOrder(){
    let self =  this;
    return new Promise((resolve, reject) => {   
      self.storage.get('user').then((user) => {
        if(user != null){ 
          let userObj = JSON.parse(user);
          let id = userObj.id; 
          let store = userObj.store; 
          self.cOrder =  new WholesaleOrder();
          //this.cOrder.salesKey = "";
          self.cOrder.salesCreateMethod = "ADMIN_APP";
        // this.cOrder.extSalesKey = "";
          self.cOrder.seller =  new WholesaleSeller(); 
          self.cOrder.seller.id = id;
          self.cOrder.storeId = store;
          self.cOrder.wholesaleCompany = new WholesaleCompany();
          //this.cOrder.deliveryAddress = new DeliveryAddress();
          //this.cOrder.invoiceAddress = new InvoiceAddress();
          self.cOrder.products = []; 
          self.cOrder.payment = {} as Payment;
          self.cOrder.payment.gross = 0;
          self.cOrder.payment.paymentRegulations =[];
          self.cOrder.payment.deposits = [];
          self.cOrder.transportPaymentStatus = new TransportPaymentStatus(); 
          self.cOrder.transportMethod = new TransportMethod();
          self.cOrder.transportCompany = new TransportCompany(); 
          self.cOrder.extraClientInfo = "";
          self.cOrder.comments = "";
          self.cOrder.preparation = new Preparation();
          self.cOrder.preparation.attendVirement = false;
          self.cOrder.preparation.grandColis = false;
          self.cOrder.preparation.mettreACote = false;
          self.cOrder.preparation.packing = false;
          self.cOrder.preparation.toutEnCarton = false;
          self.cOrder.process = 0;
          self.productProvider.setProductListForOrder([]);
        }else{ 
        } 
      });  
   
    });
  }

  setCurrentOrder (order){
    this.cOrder = order;
  }

  getCurrentOrder (){
    return this.cOrder;
  }

  submitOrder(orderObj : WholesaleOrder){
    return new Promise((resolve, reject) => { 
      let order = new WholesaleOrderSubmit(orderObj);
      let headers = new HttpHeaders().set('Content-Type', 'application/json'); 
      headers = headers.append("Accept", "application/json");   
      let url = ENV.BASEURL   + "/admin/wholesale/order";  
      this.http.post(url, order, {headers})
        .subscribe(data => {
          resolve();
          console.log(data['_body']);
        }, error => {
          reject();
          console.log(error);
        });
   });
  } 
}
 