import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { endpoints as ENV } from '../../app/environment';

 
@Injectable()
export class LogisticProvider {

  constructor(public http: HttpClient) {
    console.log('Hello LogisticProvider Provider');
  }
  getTransportCompanies(page,pSize){ 
    let url = ENV.BASEURL + "/admin/transportCompanies?";  
    let url_paramaters = "p="+ page +
                         "&ps="+ pSize ; 
    return new Promise((resolve, reject) => { 
         this.http.get(url + url_paramaters)
        .subscribe(
          data =>  resolve(data['list']),
          error => reject(error)
        );
    });
  }
}
