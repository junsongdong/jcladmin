import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';  
import { HttpHeaders } from '@angular/common/http';
import {
  Http,
  Headers,
  Response, 
  RequestOptions
} from '@angular/http';
import { endpoints as ENDPOINTS } from '../../app/environment';
import { WholesaleCompany, WholesaleCompanyAddress } from '../../app/models/wholesale.company.model';
import { endpoints as ENV } from '../../app/environment';

@Injectable()
export class ClientProvider {
  public cWholesaleCompany : WholesaleCompany;
  public cClientAddress : WholesaleCompanyAddress;

  constructor(public http: HttpClient) {
    console.log('Hello ClientProvider Provider');
    if(!this.cWholesaleCompany )
    this.cWholesaleCompany =  new WholesaleCompany();
    if(!this.cClientAddress )
    this.cClientAddress =  new WholesaleCompanyAddress();
  }

  searchClients(keyword,page,pSize){ 
    let url = ENDPOINTS.BASEURL + "/admin/wholesale/companies/query?";  
    let url_paramaters = "k="+ keyword +
                         "&p="+ page +
                         "&ps="+ pSize ; 
    return new Promise((resolve, reject) => { 
         this.http.get(url + url_paramaters)
        .subscribe(
          data =>  resolve(data),
          error => reject(error)
        );
    });
  }

  setCurrentWholesaleCompany(client){ 
   this.cWholesaleCompany = client;
  }
  setCurrentWholesaleCompanyAddress(address){ 
    this.cClientAddress = address;
   }
  create(){

  }
  sendClientInfo(type){  
    let headers = new HttpHeaders().set('Content-Type', 'application/json'); 
    headers = headers.append("Accept", "application/json"); 
    let params = new URLSearchParams();
    params.append("code", encodeURIComponent(this.cWholesaleCompany.code));
    params.append("civility", encodeURIComponent(this.cWholesaleCompany.civility));
    params.append("email", encodeURIComponent(this.cWholesaleCompany.email));
    params.append("name", encodeURIComponent( this.cWholesaleCompany.name));
    params.append("type", encodeURIComponent( this.cWholesaleCompany.type));
    params.append("billingAddress", encodeURIComponent( this.cWholesaleCompany.billingAddress));
    params.append("deliveryAddress", encodeURIComponent( this.cWholesaleCompany.deliveryAddress));
    params.append("taxType", encodeURIComponent( this.cWholesaleCompany.taxType));
    params.append("type", encodeURIComponent( this.cWholesaleCompany.type)); 
    params.append("siret", encodeURIComponent( this.cWholesaleCompany.siret));
    params.append("tvaNumber", encodeURIComponent( this.cWholesaleCompany.siret));
    params.append("telephone", encodeURIComponent( this.cWholesaleCompany.telephone));
    params.append("address", encodeURIComponent( this.cWholesaleCompany.address.toString()));
    params.append("riskCodeId", encodeURIComponent( this.cWholesaleCompany.riskCodeId));
    let postData = { 
      "code": this.cWholesaleCompany.code,
      "civility": this.cWholesaleCompany.civility,
      "email": this.cWholesaleCompany.email,
      "name":this.cWholesaleCompany.name,
      "type": this.cWholesaleCompany.type,
      "billingAddress":this.cWholesaleCompany.billingAddress,
      "deliveryAddress": this.cWholesaleCompany.deliveryAddress,
      "taxType":this.cWholesaleCompany.taxType,
      "siret": this.cWholesaleCompany.siret,
      "tvaNumber": this.cWholesaleCompany.tvaNumber,
      "telephone": this.cWholesaleCompany.telephone,
      "address":  this.cWholesaleCompany.address,
      "riskCodeId": this.cWholesaleCompany.riskCodeId,
    }
    //let postData = params.toString();
      let url = ENV.BASEURL   + "/admin/wholesale/company";  
      if(type == "create"){ 
        return new Promise((resolve, reject) => { 
          this.http.post(url, postData, {headers})
          .subscribe(data => {
            resolve(data);
          }, error => {
            reject();
          });
        });
       
      }else{ 
        return new Promise((resolve, reject) => { 
          this.http.put(url, postData, {headers})
          .subscribe(data => {
            resolve(data);
          }, error => {
            reject();
          }); 
        });
     
      } 
  }
  tvaValidate(tvaNumber){ 
    let access_key = "b0d5300f807aabb5ab15e57da1a3fb9f";
    let url = "http://www.apilayer.net/api/validate?";  
    let url_paramaters = "&access_key="+ access_key +
                          "&vat_number="+ tvaNumber ; 
    "&vat_number="+ tvaNumber; 
    return new Promise((resolve, reject) => { 
         this.http.get(url + url_paramaters)
        .subscribe(
          data =>  resolve(data),
          error => reject(error)
        );
    });
  }
}
