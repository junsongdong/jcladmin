import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { endpoints as ENV } from '../../app/environment';

@Injectable()
export class ProductProvider {
  public productListForOrder : any;
  constructor(public http: HttpClient) {
    console.log('Hello ProductProvider Provider');
    this.productListForOrder = [];
  }
  
  getProductByCode(productCode){ 
    let url = ENV.BASEURL + "/admin/product/summary?";  
    let url_paramaters = "productCode="+ productCode; 
    return new Promise((resolve, reject) => { 
         this.http.get(url + url_paramaters)
        .subscribe(
          data =>  resolve(data),
          error => reject(error)
        );
    });
  }

  setProductListForOrder(list){
    this.productListForOrder = list;
  }

  getProductListForOrder(){
    return this.productListForOrder;
  }

  resetProductListForOrder(){
    this.productListForOrder = [];
  } 

  deleteProductForOrder(list , product){
    let index = list.findIndex(item => item.id == product.id);
    list.splice(index, 1);
  }
}
