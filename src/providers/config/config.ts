import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { endpoints as ENV } from '../../app/environment';
import { Storage } from '@ionic/storage';

@Injectable()
export class ConfigProvider {
  public configData : any;
  public groups : any;
  public appData : any;
  constructor(public http: HttpClient,
    public storage : Storage) {
    console.log('Hello ConfigProvider Provider');
    this.appData = null;
  }

  getConfigDataRemote (){ 
    return new Promise((resolve, reject) => {  
         this.http.get(ENV.BASEURL + "/config")
        .subscribe(
          data => {
            resolve(data);
           // this.storage.set('config',data ); 
            this.configData = data;
          },
          error => reject(error)
        );
    });  
  }
  saveConfigData (config){ 
    this.storage.set('config',config ); 
  }

  getGroupRemote (){ 
    return new Promise((resolve, reject) => {  
         this.http.get(ENV.BASEURL + "/services")
        .subscribe(
          data => {
            resolve(data); 
            this.groups = data['data'];
          },
          error => reject(error)
        );
    });  
  }  

  getStoresRemote(){
    return new Promise((resolve, reject) => {  
      this.http.get(ENV.BASEURL + "/admin/companies?p=1&ps=100")
     .subscribe(
       data => {
          resolve(data); 
        },
        error => reject(error)
      );
     });   
  }
  getAppData(){ 
    return new Promise((resolve, reject) => {  
      let tasks = [];
      tasks.push(this.getConfigDataRemote());
      tasks.push(this.getGroupRemote());
    //  tasks.push(this.getStoresRemote());
      this.getFinalData(tasks).then((result) => {
        console.log("getConfigDataRemote OK ");  
        this.appData =  result;
        resolve(result);
      }).catch((err) => {
        reject(err);
      }); 
    });   
  }

  getCurrentAppData (){ 
    return this.appData;
  }
  getInSequence(tasks){
    let data = [];
    return  tasks.reduce((promise,item) => {
      return promise.then(() => { 
        return new Promise((resolve, reject)=> {
          data.push(resolve(data)); 
          }).catch((err) => {
            
          }); 
      })
    },Promise.resolve())
  }
 getFinalData(arr) {
    let data = [];
    return arr.reduce((promise, item) => {
      return promise
        .then((result) => { 
          return this.asyncFunc(item).then(result => {
            data.push(result) 
            return data
          })
        })
        .catch(console.error)
    }, Promise.resolve())
  }
  asyncFunc(e) {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(e), e * 1000);
    });
  }
  getStores(){
    let stores   =  [
      {
        value : "73436c17-c64f-426e-ae10-fc7d9b81e252",
        name   : "BY JULIE"
      },
      {
        value : "05c605a4-ac1f-42e4-8a76-8b8931ee37f0",
        name   : "JCL"
      },
      {
        value : "cc403259-ec4b-4bea-83c4-ec0791c03c23",
        name   : "zhanlai"
      }
    ]; 
    return stores;
  }
}
