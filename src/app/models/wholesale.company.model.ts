export class WholesaleCompany { 
    code: string;
    civility: string;
    email: string;
    name: string;
    type: string;
    billingAddress: string;
    deliveryAddress: string;
    taxType: string;
    siret: string;
    tvaNumber: string;
    telephone: string;
    address : WholesaleCompanyAddress [];
    riskCodeId : string;
}

export class WholesaleCompanyAddress {
    id: string;
    telephone: string;
    receiverName: string;
    countryCode: string;
    countryName: string;
    city: string;
    address1: string;
    address2: string;
    postalCode: string;
    ifBillingAddress : boolean;
    ifDeliveryAddress : boolean;
    
}
