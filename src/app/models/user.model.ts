export class User {
    username: string;
    token: string; 
    id: string;
    store: string;
    language: string
}
 