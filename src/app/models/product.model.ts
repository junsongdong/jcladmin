 
 export class ProductForOrder { 
    id: any;
    productCode: any;
    totalCountOfPiece: any;
    price : Price = {} as any;
    orderedList : Array<object>;
    count : any;
    total : any;
    children: SaleType[];
    constructor (productObj = {}){
       this.id =  new Date().getTime().toString();
       this.productCode =  "";
       this.totalCountOfPiece = productObj["totalCountOfPiece"];
       if(productObj["price"])
       this.price = productObj["price"];
       else{
           this.price.value = "0";
       }
       this.orderedList = [];
       this.total = 0;
       this.count = 0;
       this.children = [];
       // for piece
       let pieceObj = {} as any;
       pieceObj.name = "件";
       pieceObj.unit= "piece";
       pieceObj.children =  []; 
       for (let key in productObj["piece"]) {
        let pieceColorObj = {} as any;
        pieceColorObj.colorNo = key;
        pieceColorObj.translation = productObj["piece"][key].translation;
        pieceColorObj.sizes =  []; 
        for (let sizeKey in productObj["piece"][key].cube) {
            let sizeObj = {} as any;
            sizeObj.sizeId = sizeKey;
            sizeObj.name = productObj["piece"][key].cube[sizeKey].name;
            sizeObj.count = 0;
            pieceColorObj.sizes.push(sizeObj);
        }
        pieceObj.children.push(pieceColorObj);
      }
   
    // for bags
    let bagObj = {} as any;
    bagObj.name = "包";
    bagObj.unit= "bag";
    bagObj.children =  [];   
    for (let bagIndex in productObj["bags"]) { 
        let bagPropObj  = {} as any; 
        bagPropObj.productCode =  productObj["bags"][bagIndex]["productCode"];
        bagPropObj.pieceInBag = productObj["bags"][bagIndex]["pieceInBag"]; 
        bagPropObj.count = 0;
        bagPropObj.children =  [];   
     for (let colorKey in productObj["bags"][bagIndex]["cube"]) {
        let bagColorObj = {} as any; 
        bagColorObj.colorNo = colorKey;
        bagColorObj.translation = productObj["bags"][bagIndex]["cube"][colorKey]["translation"];
        bagColorObj.sizes =  []; 
        for (let sizeKey in productObj["bags"][bagIndex]["cube"][colorKey]["cube"]) { 
            let sizeObj = {} as any;
            sizeObj.sizeId = sizeKey;
            sizeObj.name = productObj["bags"][bagIndex]["cube"][colorKey]["cube"][sizeKey].name;
            sizeObj.value = productObj["bags"][bagIndex]["cube"][colorKey]["cube"][sizeKey].value;
            sizeObj.count = 0;
            bagColorObj.sizes.push(sizeObj);
        }
        bagPropObj.children.push(bagColorObj); 
     }  
     bagObj.children.push(bagPropObj);
   } 

    // for box 
    let boxObj = {} as any;
    boxObj.name = "箱";
    boxObj.unit= "box";
    boxObj.children =  [];   
    for (let boxIndex in productObj["boxes"]) { 
      let boxPropObj  = {} as any; 
      boxPropObj.productCode = productObj["boxes"][boxIndex]["productCode"];
      boxPropObj.bagInbox = productObj["boxes"][boxIndex]["bagInbox"];
      boxPropObj.totalPiece = productObj["boxes"][boxIndex]["totalPiece"];
      boxPropObj.count = 0;
      boxPropObj.children =  [];    
     for (let colorKey in productObj["boxes"][boxIndex]["cube"]) {
        let boxColorObj = {} as any; 
        boxColorObj.colorNo = colorKey;
        boxColorObj.translation = productObj["boxes"][boxIndex]["cube"][colorKey]["translation"];
        boxColorObj.sizes =  []; 
        for (let sizeKey in productObj["boxes"][boxIndex]["cube"][colorKey]["cube"]) { 
            let sizeObj = {} as any;
            sizeObj.sizeId = sizeKey;
            sizeObj.name = productObj["boxes"][boxIndex]["cube"][colorKey]["cube"][sizeKey].name;
            sizeObj.count = 0;
            sizeObj.value = productObj["boxes"][boxIndex]["cube"][colorKey]["cube"][sizeKey].value;
            boxColorObj.sizes.push(sizeObj);
        }
        boxPropObj.children.push(boxColorObj);
     }  
     boxObj.children.push(boxPropObj);
   } 
   this.children.push(pieceObj);
   this.children.push(bagObj);
   this.children.push(boxObj); 

 }
}
export class Price {  
    currency: string;
    value: string; 
}
export class SaleType { 
    name : string;
    unit: string;
    children: any = [] ;
}
 