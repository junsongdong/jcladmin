 
import { WholesaleCompany } from "./wholesale.company.model";
import { ProductForOrder } from "./product.model";

export class WholesaleOrder {  
    salesCreateMethod: string; 
    seller: WholesaleSeller;
    storeId: string;
    wholesaleCompany: WholesaleCompany; 
    products: ProductForOrder[]; 
    payment : Payment;
    transportPaymentStatus : TransportPaymentStatus;
    transportMethod : TransportMethod;
    transportCompany : TransportCompany; 
    extraClientInfo : string;
    comments : string;
    preparation :Preparation;
    process : number;
   
}
 
export class DeliveryAddress { 
    id: string;
    telephone: string;
    receiverName: string;
    countryCode: string;
    city: string;
    address1: string;
    address2: string;
    postalCode: string; 
}
export class InvoiceAddress { 
    id: string;
    telephone: string;
    receiverName: string;
    countryCode: string;
    city: string;
    address1: string;
    address2: string;
    postalCode: string; 
}

export class Product { 
    code: string;
    proportionCode: string;
    salePriceByPiece: number;
    unit: string;
    count: number;
    pieceCount: number;
    colorNo: string;
    colorName: {}; 
    sizeId : string;
    sizeName : string;
    cube : {};
    fabric :{};
    images : string[] = []
}

export class Payment { 
    gross: number;
    discount: number;
    paymentDiscount: number;
    discountPercentage : number;
    tax: number;
    net: number;
    transportCost: number;
    total: number;
    restToPay: number; 
    deposits: Deposit[]; 
    paymentRegulations: PaymentRegulation[]; 
}

export class Deposit {
    id: string;
    name: string;
    prefix: string; 
    paymentType: PaymentType;
    currency: string;
    value: number;
    payDate: string; 
}

export class PaymentType {
    id: string;
    translation: Translation; 
}

export class PaymentRegulation {
    id : string;
    paymentType: PaymentType;
    currency: string;
    value: number;
    payDate: string; 
}


export class WholesaleSeller {
    id: string 
}
 
export class TransportPaymentStatus { 
    id: string;
    translation : Translation;
 
} 
export class TransportMethod { 
    id: string;
    translation : Translation;
 
} 
export class Translation { 
    additionalProp1: string;
    additionalProp2: string;
    additionalProp3: string;
} 

export class TransportCompany { 
    id: string;
    name : string;
    telephone: string;
    fax : string;
    address : string;
}  
export class Preparation { 
    grandColis: boolean;
    packing : boolean;
    toutEnCarton: boolean; 
    attendVirement : boolean;
    mettreACote : boolean;
}   
  
