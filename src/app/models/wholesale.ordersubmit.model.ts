 
import { Preparation, WholesaleOrder, Product } from "./wholesale.order.model";

export class WholesaleOrderSubmit {  
    salesCreateMethod: string; 
    seller: WholesaleSellerSubmit;
    storeId: string;
    wholesaleCompany: WholesaleCompanySubmit; 
    products: ProductSubmit[]; 
    payment : PaymentSubmit;
    transportPaymentStatus : TransportPaymentStatusSubmit;
    transportMethod : TransportMethodSubmit;
    transportCompany : TransportCompanySubmit; 
    extraClientInfo : string;
    comments : string;
    preparation :Preparation ;
    process : number;
    constructor( wholesaleOrder : WholesaleOrder){
        this.salesCreateMethod = wholesaleOrder.salesCreateMethod;
        this.seller = new WholesaleSellerSubmit();
        this.seller.id =  wholesaleOrder.seller.id;
        this.storeId = wholesaleOrder.storeId;
        this.wholesaleCompany =  new WholesaleCompanySubmit();
        this.wholesaleCompany.code = wholesaleOrder.wholesaleCompany.code;
        this.products = []; 
        wholesaleOrder.products.forEach(productForOrder => {
            productForOrder.orderedList.forEach(prod => {
                let prodObj =  new ProductSubmit();
                prodObj.code = productForOrder.productCode;
                prodObj.proportionCode = productForOrder.productCode;
                prodObj.unit = prod['unit'];
                prodObj.count = prod['count'];
                prodObj.sizeId = prod['sizeId'];
                prodObj.colorNo = prod['colorNo'];
                prodObj.pieceCount = prod['pieceCount'];
                prodObj.salePriceByPiece = Number(productForOrder.price.value);
                this.products.push(prodObj);
            });  
        });
        this.payment = new PaymentSubmit();
        this.payment.gross = wholesaleOrder.payment.gross;
        this.payment.discount = wholesaleOrder.payment.discount;
        this.payment.paymentDiscount = wholesaleOrder.payment.paymentDiscount;
        this.payment.tax = wholesaleOrder.payment.tax;
        this.payment.net = wholesaleOrder.payment.net;
        this.payment.transportCost = wholesaleOrder.payment.transportCost;
        this.payment.total = wholesaleOrder.payment.total;
        this.payment.restToPay = wholesaleOrder.payment.restToPay;
        this.payment.deposits =  new Array<DepositSubmit>();
        wholesaleOrder.payment.deposits.forEach(element => {
               let deposit =  new DepositSubmit();
               deposit.currency =  element.currency;
               deposit.value =  element.value;
               deposit.paymentType =  {} as any;
               deposit.paymentType["id"] =  element.paymentType["id"];
               this.payment.deposits.push(deposit);
        });
        this.payment.paymentRegulations =  new Array<PaymentRegulationSubmit>();
        wholesaleOrder.payment.paymentRegulations.forEach(element => {
            let regulation =  new PaymentRegulationSubmit();
            regulation.currency =  element.currency; 
            regulation.value =  element.value; 
            regulation.paymentType =  {} as any;
            regulation.paymentType["id"] =  element.paymentType["id"];
            this.payment.paymentRegulations.push(regulation);
       });
       this.transportPaymentStatus =  new TransportPaymentStatusSubmit();
       this.transportPaymentStatus.id = wholesaleOrder.transportPaymentStatus.id;
       this.transportCompany =  new TransportCompanySubmit();
       this.transportCompany.id = wholesaleOrder.transportCompany.id;
       this.transportMethod =  new TransportMethodSubmit();
       this.transportMethod.id = wholesaleOrder.transportMethod.id;
       this.extraClientInfo =  wholesaleOrder.extraClientInfo;
       this.comments =  wholesaleOrder.comments;
       this.preparation = new Preparation();
       this.preparation =  wholesaleOrder.preparation;
       this.process =  wholesaleOrder.process;
    }
}
   
export class WholesaleCompanySubmit { 
    code: string; 
}
export class ProductSubmit { 
    code: string;
    proportionCode: string;
    salePriceByPiece: number;
    unit: string;
    count: number;
    pieceCount: number;
    colorNo: string; 
    sizeId : string; 
}

export class PaymentSubmit { 
    gross: number;
    discount: number;
    paymentDiscount: number;
    tax: number;
    net: number;
    transportCost: number;
    total: number;
    restToPay: number; 
    deposits: DepositSubmit[]; 
    paymentRegulations: PaymentRegulationSubmit[]; 
}

export class DepositSubmit { 
    paymentType: PaymentTypeSubmit[];
    currency: string;
    value: number; 
}

export class PaymentTypeSubmit {
    id: string; 
}

export class PaymentRegulationSubmit {
    currency: string;
    value: number; 
    paymentType: PaymentTypeSubmit;
   
} 

export class WholesaleSellerSubmit {
    id: string 
}
 
export class TransportPaymentStatusSubmit { 
    id: string; 
} 
export class TransportMethodSubmit { 
    id: string;  
} 
 
export class TransportCompanySubmit { 
    id: string 
}  
  
