import { AuthProvider } from './../providers/auth/auth';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler, Platform } from 'ionic-angular';
import { MyApp } from './app.component'; 
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateModule,TranslateLoader } from '@ngx-translate/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader'; 
import { ComponentsModule } from "../components/components.module";
import { ClientProvider } from '../providers/client/client';
import { OrderProvider } from '../providers/order/order';  
import {HttpModule, XHRBackend, RequestOptions, Http} from '@angular/http'; 
import {Storage, IonicStorageModule} from '@ionic/storage';
import { UserProvider } from '../providers/user/user';
import { StorageProvider } from '../providers/storage/storage';  
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../providers/auth/tokenInterceptor';
import { ConfigProvider } from '../providers/config/config';
import { LogisticProvider } from '../providers/logistic/logistic';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { ProductProvider } from '../providers/product/product';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
export function setTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
 }
 
@NgModule({
  declarations: [
    MyApp 
  ],
  imports: [
    BrowserModule, 
    ComponentsModule,
    IonicStorageModule.forRoot(),
    HttpModule,  
    IonicModule.forRoot(MyApp,{  
      backButtonText : '',
      statusbarPadding: true,
      tabsHideOnSubPages: true}),
    HttpClientModule,
    TranslateModule.forRoot({ 
    loader: {
      provide: TranslateLoader,
      useFactory: (setTranslateLoader),
      deps: [HttpClient]
      }
    }),
    SelectSearchableModule,
    NgxQRCodeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp  
  ],
  providers: [
    StatusBar,
    SplashScreen,
    QRScanner,
    BarcodeScanner,
    {provide: ErrorHandler, useClass: IonicErrorHandler}, 
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },  
    AuthProvider,
    ClientProvider,
    OrderProvider,
    UserProvider,
    StorageProvider,
    ConfigProvider,
    LogisticProvider,
    ProductProvider
  ]
})
export class AppModule {}
