import { Component } from '@angular/core';
import { App,Platform,AlertController, Keyboard } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { AuthProvider } from '../providers/auth/auth';
import { UserProvider } from '../providers/user/user';
import { Storage } from '@ionic/storage'; 
import 'rxjs/add/operator/mergeMap'; 
import { ConfigProvider } from '../providers/config/config';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  // rootPage:any = 'CreateOrderClientPage';
  //rootPage:any = 'TabsPage';
   rootPage:any = 'LoginPage';
   private onResumeSubscription: Subscription;
  constructor(platform: Platform, 
    public app : App,
    statusBar: StatusBar, 
    keyboard : Keyboard,
    splashScreen: SplashScreen,
    public authProvider:AuthProvider,
    public userProvider:UserProvider, 
    public storage :Storage,
    public configProvider :ConfigProvider,
    private alertCtrl: AlertController,
    translateService: TranslateService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      statusBar.overlaysWebView(true); 
      keyboard.hideFormAccessoryBar(false); 
      statusBar.styleLightContent();
      splashScreen.hide();
      translateService.setDefaultLang('cn');   
      this.checkAuth(); 
    });

    platform.registerBackButtonAction(() => {
      let nav = app.getActiveNavs()[0];
      let activeView = nav.getActive(); 
      if(activeView != null){
        if(nav.canGoBack())
          nav.pop();
        else if (typeof activeView.instance.backButtonAction === 'function')
          activeView.instance.backButtonAction();
        else
          nav.parent.select(0); // goes to the first tab
      }
    }); 
    this.onResumeSubscription = platform.resume.subscribe(() => {
      this.checkAuth();
   });
  } 
   
  checkAuth(){ 
    let self = this;
    this.storage.get('user').then( (result) =>{ 
      self.authProvider.login().then((data: Response) => {
        self.app.getActiveNav().setRoot('TabsPage'); 
      },
      (err) => {
        console.log(err);  
      }); 
     }  
    ); 
 } 
}
