import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';    
import { CommonModule }   from '@angular/common';
import {IonicModule} from 'ionic-angular';
import { OrderHeaderComponent } from './order-header';
 
@NgModule({
	declarations: [   
    OrderHeaderComponent 
    ],
	imports: [ 
        IonicModule,
        TranslateModule,
        CommonModule 
    ],
	exports: [   
    OrderHeaderComponent 
    ]
})
export class OrderHeaderComponentModule {}
