import { Component ,Input} from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
 
@Component({
  selector: 'order-header',
  templateUrl: 'order-header.html'
})
export class OrderHeaderComponent {

  @Input()
  public headerTitle: string;
  @Input()
  public noChange: Function; 
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad HeaderPage');
  }
  goToHomePage(): void {
    this.setNoChange();
    let self = this;
    self.navCtrl.setRoot('TabsPage');
    self.navCtrl.popToRoot({ animate: false });
  } 
  
  setNoChange(){
    this.noChange();
  }
}
