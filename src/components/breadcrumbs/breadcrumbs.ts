import { Component ,Input} from '@angular/core';
import {  NavController, Platform, NavParams } from 'ionic-angular';
 
 
@Component({
  selector: 'breadcrumbs',
  templateUrl: 'breadcrumbs.html'
})
export class BreadcrumbsComponent { 
  @Input()
  public pageNumber: string; 
  constructor( public navCtrl: NavController) {
    console.log('Hello BreadcrumbsComponent Component'); 
  }
  goToScreen(screenNumber, bool) { 
    switch (screenNumber) {
      case 1: 
        if(screenNumber != this.pageNumber)  
        this.navCtrl.push('CreateOrderClientPage', {}, { animate: false } ).then(() => {
          const startIndex = this.navCtrl.getActive().index - 1;
          this.navCtrl.remove(startIndex, 1);
        }); 
        break;
      case 2:
      if(screenNumber != this.pageNumber)  
      this.navCtrl.push('CreateOrderProductPage',{}, { animate: false } ).then(() => {
        const index = this.navCtrl.getActive().index;
        this.navCtrl.remove(0, index);
        }); 
        break;
      case 3:
      if(screenNumber != this.pageNumber)  
      this.navCtrl.push('CreateOrderPaymentPage',{}, { animate: false } ).then(() => {
        const index = this.navCtrl.getActive().index;
        this.navCtrl.remove(0, index);
        });
        break; 
      case 4: 
      if(screenNumber != this.pageNumber)  
      this.navCtrl.push('CreateOrderLogisticPage',{}, { animate: false } ).then(() => {
        const index = this.navCtrl.getActive().index;
        this.navCtrl.remove(0, index);
        });
        break; 
      default:
        
    }
  }
}
 
