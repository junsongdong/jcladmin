import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';   
import { CommonModule }   from '@angular/common';
import {IonicModule} from 'ionic-angular';
import { BreadcrumbsComponent } from './breadcrumbs';
 
@NgModule({
	declarations: [  
    BreadcrumbsComponent,
    ],
	imports: [ 
        IonicModule,
        TranslateModule,
        CommonModule 
    ],
	exports: [  
    BreadcrumbsComponent ]
})
export class BreadcrumbsComponentModule {}
