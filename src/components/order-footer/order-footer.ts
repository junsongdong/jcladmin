import { Component,Input, Output, EventEmitter } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular'; 
 
@Component({
  selector: 'order-footer',
  templateUrl: 'order-footer.html'
})
export class OrderFooterComponent {

  @Input()
  nextScreen: string;
  @Input()
  public total: string; 
  @Input()
  public onOrderSubmit: Function; 
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FooterPage');
  }

  goToNextScreen() {
    console.log(this.nextScreen);
    this.navCtrl.push(this.nextScreen, {}, { animate: false });
  }

  goToPreviousScreen() {
    switch (this.nextScreen) {
      case "CreateOrderPaymentPage": 
       this.navCtrl.push('CreateOrderClientPage',{}, { animate: false } ).then(() => {
        const index = this.navCtrl.getActive().index;
        this.navCtrl.remove(0, index);
        });
        break;
      case "CreateOrderPaymentPage": 
        this.navCtrl.push('CreateOrderClientPage',{}, { animate: false } ).then(() => {
          const index = this.navCtrl.getActive().index;
          this.navCtrl.remove(0, index);
          });
        break; 
      case "NoPage": 
        this.navCtrl.push('CreateOrderPaymentPage',{}, { animate: false } ).then(() => {
          const index = this.navCtrl.getActive().index;
          this.navCtrl.remove(0, index);
          });
        break;
      case "CreateOrderLogisticPage": 
        this.navCtrl.push('CreateOrderProductPage',{}, { animate: false } ).then(() => {
          const index = this.navCtrl.getActive().index;
          this.navCtrl.remove(0, index);
          });
        break; 
      default: 
        this.navCtrl.push('CreateOrderClientPage',{}, { animate: false } ).then(() => {
          const index = this.navCtrl.getActive().index;
          this.navCtrl.remove(0, index);
          });
    }
  }
  
  showPrevious(): boolean {
    if (this.nextScreen == "CreateOrderProductPage")
      return false;
    else
      return true;
  }

  showNext(): boolean {
    if (this.nextScreen == "NoPage")
      return false;
    else
      return true;
  }
  ifSubmitPage(): boolean{
    if (this.nextScreen == "NoPage")
    return true;
    else
    return false;
  }
  ifProductPage(): boolean{
    if (this.nextScreen == "CreateOrderPaymentPage")
    return true;
    else
    return false;
  }

  onSubmitClick(){
    this.onOrderSubmit();
  }

}
