import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';   
import { CommonModule }   from '@angular/common';
import {IonicModule} from 'ionic-angular';
import { OrderFooterComponent } from './order-footer';
 
@NgModule({
	declarations: [   
    OrderFooterComponent],
	imports: [ 
        IonicModule,
        TranslateModule,
        CommonModule 
    ],
	exports: [   
    OrderFooterComponent]
})
export class OrderFooterComponentModule {}
