import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';   
import { CommonModule }   from '@angular/common';
import {IonicModule} from 'ionic-angular';
import { AccordionListComponent } from './accordion-list';
 
@NgModule({
	declarations: [   
        AccordionListComponent],
	imports: [ 
        IonicModule,
        TranslateModule,
        CommonModule 
    ],
	exports: [   
        AccordionListComponent]
})
export class AccordionListComponentModule {}
